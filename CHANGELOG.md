# Changelog de grahes_en_svg
- Tenir un changelog : https://www.spip.net/fr_article6825.html
- Ecrire un commit : https://www.spip.net/fr_article6824.html

## [Unreleased]

### CHANGED
- Mise en conformité des fichiers de langue. Adopter la syntaxe valide à partir de SPIP 4.1 ce qui facilitera la migration en SPIP 5

### ADDED

- `CHANGELOG.md`
