# graphes_en_svg

Sans librairie en javascript, produire des graphes avec les SVG

## MODELE svg_barres_horizontales

- Fichier : `../modeles/svg_barres_horizontales.html`
- Format du conteneur recommandé : 1x1

argument nécessaire :
@param `data`   string | array données chiffrées

arguments utilisés :
@param `labels` string | array les légendes correspondant aux données
@param `texte` string texte affiché avec le graphe. `pct` pour la valeur relative en pourcentage. `valeur` pour la donnée brute.
@param `colors` string | array les couleurs pour chaque donnée
@param `recadre` string par défaut recadre le graph
@param `opacity` string transparence entre zéro et un
@param `objet` string ombre si oui
@param `ratio` string adaptation du graph. Par défaut prend la forme du récipient `none`.

## MODELE svg_barres_verticales

- Fichier : `../modeles/svg_barres_verticales.html`
- Format du conteneur recommandé : 1x1

argument nécessaire :
@param `data`   string | array données chiffrées

arguments utilisés :
@param `labels` string | array les légendes correspondant aux données
@param `texte` string texte affiché avec le graphe. `pct` pour la valeur relative en pourcentage. `valeur` pour la donnée brute.
@param `colors` string | array les couleurs pour chaque donnée
@param `recadre` string par défaut recadre le graph
@param `opacity` string transparence entre zéro et un
@param `objet` string ombre si oui
@param `ratio` string adaptation du graph. Par défaut prend la forme du récipient `none`.

## MODELE svg_lignes

- Fichier : `../modeles/svg_lignes.html`
- Format du conteneur recommandé : 2x1

argument nécessaire :
@param `data`   string | array données chiffrées

arguments utilisés :
@param `labels` string | array les légendes correspondant aux données
@param `texte` string texte affiché avec le graphe. `labels` pour afficher les labels dans l'axe y.
@param `colors` string | array les couleurs pour chaque donnée
@param `couleur_ligne` string couleur des lignes de repère (axe y)
@param `couleur_axe` string couleur des axes x et y
@param `recadre` string par défaut recadre le graph
@param `opacity` string transparence entre zéro et un
@param `objet` string ombre si oui
@param `ratio` string adaptation du graph. Par défaut prend la forme du récipient `none`.

## MODELE svg_anneau

- Fichier : `../modeles/svg_anneau.html`
- Format du conteneur recommandé : 1x1

argument nécessaire
@param `data`   string | array données chiffrées

arguments utilisés
@param `largeur_anneau` string largeur du donut par defaut 10
@param `trou_couleur` string | par defaut laissé vide
@param `labels` string | array les légendes correspondant aux données
@param `texte` string texte affiché avec le graphe, labels
@param `colors` string | array les couleurs pour chaque donnée
@param `recadre` string par défaut recadre le graph
@param `opacity` string transparence entre zéro et un
@param `objet` string ombre si oui
@param `ratio` string adaptation du graph par défaut prend la forme du récipient 

## MODELE svg_legendes

- Fichier : `../modeles/svg_legendes.html`
- Format du conteneur recommandé : 1x1

argument nécessaire :
@param `data`   string | array données chiffrées

arguments utilisés :
@param `labels` string | array les légendes correspondant aux données
@param `colors` string | array les couleurs pour chaque donnée